package ru.will0376.wcdiscord;

import lombok.Getter;
import net.minecraftforge.common.config.Configuration;

import java.io.File;

@Getter
public class Config {
	Configuration conf;
	String token;
	String channelId;
	String channelIdReport;
	String adminGroup;
	String prefix;
	String botName;
	boolean useReports;

	public Config(File file) {
		this.conf = new Configuration(file);
	}

	public Config init() {
		this.load();
		this.setVars();
		this.save();
		return this;
	}

	public void save() {
		conf.save();
	}

	public void setVars() {
		token = conf.getString("token", "General", "", "Token for bot get it: http://tetraquark.ru/archives/258");
		channelId = conf.getString("channelId", "General", "", "");
		channelIdReport = conf.getString("channelIdReport", "General", "", "");
		adminGroup = conf.getString("adminGroup", "General", "Moderator", "");
		prefix = conf.getString("prefix", "General", "!", "Prefix for commands.");
		botName = conf.getString("botName", "General", "Botname", "");
		useReports = conf.getBoolean("useReports", "General", false, "");
	}

	public void load() {
		conf.load();
	}
}
