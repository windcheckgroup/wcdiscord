package ru.will0376.wcdiscord;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.Channel;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.event.message.MessageCreateEvent;
import ru.will0376.wcdiscord.commands.*;

import java.awt.*;
import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Log4j2
public class DiscordController {
	public static DiscordApi api;
	public static Thread thread;
	private static final HashMap<String, Long> cooldown = new HashMap<>();

	public static void startThread() {
		thread = new Thread(new DiscordThread());
		thread.start();
	}

	public static void stopThread() {
		thread.stop();
		api = null;
	}

	public static void printToDiscordChannel(String text) {
		if (api != null) {
			getChannel().sendMessage(text);
			log.debug("New message do discord: {}", text);
		} else {
			log.error("api == null =\\");
		}
	}

	public static void printToReportChannel(String text) {
		if (WCDiscord.config.isUseReports()) {
			long time = System.currentTimeMillis();
			if (api != null) {
				if (cooldown.containsKey(text)) {
					if (!(cooldown.get(text) > time)) {
						cooldown.remove(text);
						cooldown.put(text, time + 2000);
						getReportChannel().sendMessage(text);
						log.debug("New message do discord: {}", text);
					}
				} else {
					cooldown.put(text, time + 2000);
					getReportChannel().sendMessage(text);
					log.debug("New message do discord: {}", text);
				}
			} else {
				log.error("api == null =\\");
			}
		}
	}

	static TextChannel getChannel() {
		Optional<Channel> channelById = api.getChannelById(WCDiscord.config.getChannelId());
		if (!channelById.isPresent())
			log.error("ChannelById == null");
		return channelById.get().asTextChannel().get();
	}

	static TextChannel getReportChannel() {
		return api.getChannelById(WCDiscord.config.getChannelIdReport()).get().asTextChannel().get();
	}

	@Log4j2
	public static class DiscordThread implements Runnable {

		public static void printHelpForAdmin() {
			EmbedBuilder eb = new EmbedBuilder();
			eb.setTitle(String.format("%s help", WCDiscord.MOD_NAME));
			eb.setAuthor(WCDiscord.config.getBotName(), null, DiscordController.api.getYourself()
					.getAvatar()
					.getUrl()
					.toString());
			eb.setColor(Color.red);
			eb.setColor(new Color(0xF40C0C));
			eb.setColor(new Color(255, 0, 54));

			eb.setDescription("Necessary role: " + WCDiscord.config.getAdminGroup());
			for (Handlers value : Handlers.values())
				value.getHandlerAbstract().addToDescription(eb);

			DiscordController.getChannel().sendMessage(eb);

		}

		public DiscordApi get() {
			return DiscordController.api;
		}

		@Override
		public void run() {
			Config config = WCDiscord.config;

			if (config.getToken().isEmpty())
				log.error("Token Empty");
			if (config.getChannelId().isEmpty())
				log.error("ChannelId Empty");
			if (config.getChannelIdReport().isEmpty())
				log.error("ChannelId for Report Empty");

			DiscordController.api = new DiscordApiBuilder().setToken(config.getToken()).login().join();

			api.addMessageCreateListener(event -> {
				if (event.getMessageContent().equalsIgnoreCase("!ping")) {
					event.getChannel().sendMessage("Pong!");
				}
			});

			get().addMessageCreateListener(event -> {
				registerListener(config, event);
			});

			printToDiscordChannel("I'm Alive!");
			log.info("Bot started!");
		}

		private void registerListener(Config config, MessageCreateEvent event) {
			if (event.getChannel().getId() == Long.parseLong(config.getChannelId())) {
				Message message = event.getMessage();
				String content = message.getContent();

				if (message.getAuthor().isBotUser())
					return;
				log.debug("Message from discord: {} -> {}", message.getAuthor().getName(), content);

				if (content.contains(config.getPrefix())) {
					String[] msg = message.getContent().replace("!", "").split(" ");

					boolean permissions = checkSenderPermissions(event, config);
					if (permissions) {
						try {
							Handlers handlers = Handlers.valueOf(msg[0]);
							handlers.getHandlerAbstract().execute(message);
						} catch (Exception exception) {
							printToDiscordChannel(exception.getMessage());
							exception.printStackTrace();
						}
					} else {
						printToDiscordChannel("No rights!");
					}
					message.delete();
				}
			}
		}

		private boolean checkSenderPermissions(MessageCreateEvent event, Config config) {
			AtomicBoolean ret = new AtomicBoolean(false);
			log.debug("Check permission for {} ({})", event.getMessage().getAuthor().getName(), event.getMessage()
					.getAuthor()
					.getId());
			event.getServer().get().getRoles(event.getMessage().getUserAuthor().get()).forEach(role -> {
				if (role.getName().equalsIgnoreCase(config.getAdminGroup()))
					ret.set(true);
			});
			return ret.get();
		}


		@Getter
		public enum Handlers {
			crash(new MessageCrashHandle()),
			check(new MessageScreenHandle()),
			list(new MessageListHandle()),
			modlist(new MessageModListHandle()),
			log(new MessageLogHandle()),
			help(new MessageHelpHandle()),
			clist(new MessageClassLoadedListHandle());

			MessageHandlerAbstract handlerAbstract;

			Handlers(MessageHandlerAbstract handlerAbstract) {
				this.handlerAbstract = handlerAbstract;
			}
		}
	}
}