package ru.will0376.wcdiscord.commands;

import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import ru.will0376.wcdiscord.DiscordController;

public class MessageHelpHandle extends MessageHandlerAbstract {
	@Override
	public String getUsage() {
		return "!help";
	}

	@Override
	public void execute(Message message) throws Exception {
		DiscordController.DiscordThread.printHelpForAdmin();
	}

	@Override
	public void addToDescription(EmbedBuilder embedBuilder) {
	}
}
