package ru.will0376.wcdiscord.commands;

import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import ru.will0376.wcdiscord.WCDiscord;
import ru.will0376.windcheckbridge.events.req.RequestCrashPlayerEvent;

public class MessageCrashHandle extends MessageHandlerAbstract {
	@Override
	public String getUsage() {
		return "crash <PlayerNick>";
	}

	@Override
	public void execute(Message message) throws Exception {
		String[] split = message.getContent().split(" ");
		if (split.length < 2) {
			printError("not enough arguments");
			return;
		}
		String playerNick = split[1];
		String adminNick = message.getAuthor().getName();
		sendEvent(new RequestCrashPlayerEvent(playerNick, adminNick, getRequester()));
	}

	@Override
	public void addToDescription(EmbedBuilder embedBuilder) {
		embedBuilder.addField("Crash", String.format("``%scrash <nick>`` - crash player", WCDiscord.config.getPrefix()));
	}
}
