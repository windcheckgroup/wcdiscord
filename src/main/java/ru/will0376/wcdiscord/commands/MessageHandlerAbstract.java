package ru.will0376.wcdiscord.commands;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.Event;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import ru.will0376.wcdiscord.DiscordController;
import ru.will0376.wcdiscord.WCDiscord;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.utils.Token;

public abstract class MessageHandlerAbstract {
	public abstract String getUsage();

	public abstract void execute(Message message) throws Exception;

	public abstract void addToDescription(EmbedBuilder embedBuilder);

	EntityPlayer findPlayer(String in) {
		return WindCheckBridge.aVersion.findPlayer(in);
	}

	void print(String in) {
		DiscordController.printToDiscordChannel(in);
	}

	void printError(String in) {
		DiscordController.printToDiscordChannel("[Error] " + in);
	}

	void sendEvent(Event in) {
		WindCheckBridge.aVersion.sendEvent(in);
	}

	Token.Requester getRequester() {
		return WCDiscord.requester;
	}
}
