package ru.will0376.wcdiscord.commands;

import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import ru.will0376.wcdiscord.WCDiscord;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.events.req.RequestModListEvent;

public class MessageModListHandle extends MessageHandlerAbstract {
	@Override
	public String getUsage() {
		return "modlist <PlayerNick>";
	}

	@Override
	public void execute(Message message) throws Exception {
		String[] split = message.getContent().split(" ");
		if (split.length < 2) {
			printError("not enough arguments");
			return;
		}

		String playerNick = split[1];
		String adminNick = message.getAuthor().getName();

		WindCheckBridge.aVersion.createNewToken(playerNick, adminNick, getRequester());
		sendEvent(new RequestModListEvent(playerNick, adminNick, getRequester()));
	}

	@Override
	public void addToDescription(EmbedBuilder embedBuilder) {
		embedBuilder.addField("ModList", String.format("``%smodList <nick>`` - get mod list from playerr",
				WCDiscord.config.getPrefix()));
	}
}
