package ru.will0376.wcdiscord.commands;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.management.PlayerList;
import net.minecraftforge.fml.common.FMLCommonHandler;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import ru.will0376.wcdiscord.DiscordController;
import ru.will0376.wcdiscord.WCDiscord;

public class MessageListHandle extends MessageHandlerAbstract {
	@Override
	public String getUsage() {
		return "list";
	}

	@Override
	public void execute(Message message) throws Exception {
		StringBuilder tmp = new StringBuilder();
		PlayerList playerList = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList();

		tmp.append(String.format("[%s/%s] Players online:\n", playerList.getPlayers().size(), playerList.getMaxPlayers()));

		for (EntityPlayerMP player : playerList.getPlayers())
			tmp.append(player.getName()).append("\n");

		DiscordController.printToDiscordChannel(tmp.toString());
	}

	@Override
	public void addToDescription(EmbedBuilder embedBuilder) {
		embedBuilder.addField("List", String.format("``%slist`` - Returns a list of server players",
				WCDiscord.config.getPrefix()));
	}
}
