package ru.will0376.wcdiscord;

import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.utils.AbstractCommand;

import java.util.Arrays;
import java.util.List;

public class DiscordCommand extends AbstractCommand {

	public DiscordCommand() {
		super("discord");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("reload").build(), getArgBuilder("stopThread").build(),
				getArgBuilder("startThread").build(), getArgBuilder("print").setHasArg()
				.build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().adminNick(sender.getName()).args(args).build();
		if (!WindCheckBridge.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}

		CommandLine line = getLine(args);
		if (line.hasOption("print")) {
			DiscordController.printToDiscordChannel(line.getOptionValue("print"));
		} else if (line.hasOption("reload")) {
			DiscordController.stopThread();
			DiscordController.startThread();
		} else if (line.hasOption("stopThread")) {
			DiscordController.stopThread();
		} else if (line.hasOption("startThread")) {
			DiscordController.startThread();
		}

		return build;
	}
}
