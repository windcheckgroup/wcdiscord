package ru.will0376.wcdiscord;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.utils.Module;
import ru.will0376.windcheckbridge.utils.SubCommands;
import ru.will0376.windcheckbridge.utils.Token;

@Mod(modid = WCDiscord.MOD_ID,
		name = WCDiscord.MOD_NAME,
		version = WCDiscord.VERSION,
		acceptedMinecraftVersions = "[1.12.2]",
		dependencies = "after:windcheckbridge@[3.0,)",
		acceptableRemoteVersions = "*")
public class WCDiscord {

	public static final String MOD_ID = "wcdiscord";
	public static final String MOD_NAME = "WCDiscord";
	public static final String VERSION = "@version@";
	public static final Module module = Module.builder().modid(MOD_ID).version(VERSION).setDefaultNames(MOD_NAME).build();
	public static Token.Requester requester;
	public static SubCommands command;
	public static Config config;
	@Mod.Instance(MOD_ID)
	public static WCDiscord INSTANCE;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		config = new Config(event.getSuggestedConfigurationFile()).init();
	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		requester = WindCheckBridge.aVersion.registerModule(module);
		command = WindCheckBridge.aVersion.registerNewCommand(new DiscordCommand());
		DiscordController.startThread();
	}
}