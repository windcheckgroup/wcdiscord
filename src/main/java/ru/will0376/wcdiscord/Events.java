package ru.will0376.wcdiscord;

import lombok.extern.log4j.Log4j2;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.will0376.windcheckbridge.events.CheckAndPrintEvent;
import ru.will0376.windcheckbridge.events.MessageModuleToModuleEvent;
import ru.will0376.windcheckbridge.events.ReloadEvent;
import ru.will0376.windcheckbridge.utils.Token;

@Mod.EventBusSubscriber
@Log4j2
public class Events {
	@SubscribeEvent
	public static void printDataEvent(CheckAndPrintEvent event) {
		Token token = event.getToken();
		if (token.getRequester().equals(WCDiscord.requester))
			DiscordController.printToDiscordChannel(String.format("Player %s -> %s, admin %s", token.getPlayerNick(),
					event.getText(), token.getAdminNick()));
	}

	@SubscribeEvent
	public static void reloadEvent(ReloadEvent event) {
		if (event.getModule() != null && event.getModule().equals(WCDiscord.module)) {
			WCDiscord.config.load();
		}
	}

	@SubscribeEvent
	public static void M2MEvent(MessageModuleToModuleEvent event) {
		log.debug("Message {} from {}", event.getData(), event.getFrom().getModuleName());
		if (event.getTo().equals(WCDiscord.module)) {
			if (event.getAction().equals("printToReportChannel")) {
				DiscordController.printToReportChannel(event.getData());
			}
		}
	}
}
